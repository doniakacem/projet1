<?php

namespace App\Controller;

use App\Entity\Voiture;
use App\Service\VoitureService;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class VoitureController extends AbstractController
{
    private $voitureservice;

    public function __construct(VoitureService $voitureService)
    {
        $this->voitureservice = $voitureService;
    }

    /**
     * @param VoitureService $voitureservice
     * @return string
     * lists all the project
     * @Rest\Get("/allVoiture")
     */
    public function getAllVoiture()
    {
        $data = $this->voitureservice->getAllvoiture();

        $encoders = [new JsonEncoder()];

        // On instancie le "normaliseur" pour convertir la collection en tableau
        $normalizers = [new ObjectNormalizer()];

        // On instancie le convertisseur
        $serializer = new Serializer($normalizers, $encoders);

        // On convertit en json
        $jsonContent = $serializer->serialize($data, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
        $response = new Response($jsonContent);

        $response->headers->set('Content-Type', 'application/json');

        return $response;


    }

    /**
     * @Rest\Post("/ajoutvoiture", name="ajoutvoiture")
     * @param VoitureService $voitureservice
     * @param Request $request
     *
     * @return Response
     */
    public function Ajoutvoiture(VoitureService $voitureService, request $request)
    {
        $data = $voitureService->AjoutVoiture($request);
        $this->addFlash('ajout avec success', $data);
        return new Response($data, 200, array(
            'Content-Type' => 'application/json'
        ));

    }

    /**
     * @Route(
     *     "/deletevoiture/{id}",
     *     name="deletevoiture",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     * @param int $id
     *
     * @return Response
     */

    public function delete(VoitureService $voitureService, int $id)
    {
        $data = $voitureService->delete($id);
        $this->addFlash('success', $data);
        return new Response($data, 200, array(
            'Content-Type' => 'application/json'
        ));


    }

    /**
     * @Route(
     *     "/putvoiture/{id}",
     *     name="putvoiture",
     *     methods={"PUT"},
     *     requirements={"id"="\d+"}
     * )
     *
     *
     * @return Response
     */
    public function putVoiture(VoitureService $voitureService, request $request, int $id)
    {
        $data = $voitureService->putVoiture($request, $id);
        $this->addFlash('success', $data);
        return new Response($data, 200, array(
            'Content-Type' => 'application/json'
        ));

    }

}
