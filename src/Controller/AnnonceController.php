<?php


namespace App\Controller;


use App\Entity\Annonce;
use App\Service\AnnonceService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use FOS\RestBundle\Controller\Annotations as Rest;

class AnnonceController extends AbstractController
{
    private $annonceservice;

    public function __construct(AnnonceService $annonceservice)
    {
        $this->annonceservice=$annonceservice;
    }
    /**
     * @Rest\Post("/postuler", name="postuler")
     *@param AnnonceService $annonceservice
     * @param Request $request
     *
     * @return Response
     */
    public function Postuler (AnnonceService $annonceService, request $request)
    {
        $data = $annonceService->Postuler($request);
        $this->addFlash('ajout avec success', $data);
        return new Response($data, 200, array(
            'Content-Type' => 'application/json'
        ));

    }
}