<?php

namespace App\Controller;

use App\Entity\Modelevoiture;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Service\ModeleVoitureService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class ModeleVoitureController extends AbstractController
{
    private $voitureservice;

    public function __construct(ModeleVoitureService $voitureservice)
    {
        $this->voitureservice = $voitureservice;
    }

    /**
     *
     * @return string
     * lists all the project
     * @Rest\Get("/allVoitures")
     */
    public function getAllVoitures()
    {
        $data = $this->getDoctrine()->getRepository(Modelevoiture::class)->findAll();

        $encoders = [new JsonEncoder()];

        // On instancie le "normaliseur" pour convertir la collection en tableau
        $normalizers = [new ObjectNormalizer()];

        // On instancie le convertisseur
        $serializer = new Serializer($normalizers, $encoders);

        // On convertit en json
        $jsonContent = $serializer->serialize($data, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
        $response = new Response($jsonContent);

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Rest\Post("/ajoutmodele", name="ajoutmodele")
     * @param ModeleVoitureService $voitureservice
     * @param Request $request
     *
     * @return Response
     */
    public function AjoutModele(ModeleVoitureService $voitureService, request $request)
    {
        $data = $voitureService->AjoutModele($request);
        $this->addFlash('ajout avec success', $data);
        return new Response($data, 200, array(
            'Content-Type' => 'application/json'
        ));
    }
    /**
     * @Rest\Get("/getmodele", name="ajoutmodele")
     * @param ModeleVoitureService $voitureservice
     * @param Request $request
     *
     * @return Response
     */
    public function getmodele(ModeleVoitureService $voitureService, request $request)
    {
        $data = $voitureService->getmodele();
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($data, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
        $response = new Response($jsonContent);

        $response->headers->set('Content-Type', 'application/json');

        return new Response($response, 200, array(
            'Content-Type' => 'application/json'
        ));

    }
    /**
     * @Rest\Get("/gettypebymodele/{idmodele}", name="ajoutmodele")
     * @param ModeleVoitureService $voitureservice
     * @param Request $request
     *
     * @return Response
     */
    public function gettypebymodele($idmodele,ModeleVoitureService $voitureService, request $request)
    {
        $data = $voitureService->gettypebymodele($idmodele);

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($data, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
        $response = new Response($jsonContent);
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }


    /**
     * @Route(
     *     "/deletemodele/{id}",
     *     name="delete_modele",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     * @param int $id
     *
     * @return Response
     */

    public function delete(ModeleVoitureService $voitureService, int $id)
    {
        $data = $voitureService->delete($id);
        $this->addFlash('success', $data);
        return new Response($data, 200, array(
            'Content-Type' => 'application/json'
        ));


    }

    /**
     * @Route(
     *     "/putModele/{id}",
     *     name="putmodele",
     *     methods={"PUT"},
     *     requirements={"id"="\d+"}
     * )
     *
     *
     * @return Response
     */
    public
    function putModele(ModeleVoitureService $voitureService, request $request, int $id)
    {
        $data = $voitureService->putModele($request, $id);
        $this->addFlash('success', $data);
        return new Response($data, 200, array(
            'Content-Type' => 'application/json'
        ));

    }


}
