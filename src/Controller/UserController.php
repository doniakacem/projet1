<?php

namespace App\Controller;

use App\Service\UserGenerator;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserController extends AbstractController
{

    private $userGenerator;

    public function __construct(UserGenerator $userGenerator)
    {
        $this->userGenerator = $userGenerator;
    }


    /**
     * @param UserGenerator $userGenerator
     * @return string
     * lists all the project
     * @Rest\Get("/allUsers")
     */
    public function getAllUsers()
    {
        $data = $this->userGenerator->getAllUsers();
        $reponse = $this->get('serializer')->serialize($data,'json');
        return new Response($reponse, 200, array(
            'Content-Type' => 'application/json'
        ));
        return $reponse;
    }

    /**
     * @Rest\Post("/inscription", name="security_registration")
     *@param UserGenerator $userGenerator
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function inscription(UserGenerator $userGenerator, request $request,  UserPasswordEncoderInterface $encoder)
    {


        $data = $userGenerator->inscription($request,$encoder);
        $this->addFlash('success', $data);
        return new Response($data, 200, array(
            'Content-Type' => 'application/json'
        ));

    }

    /**
     * @Route(
     *     "/deleteuser/{id}",
     *     name="delete_user",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     * @param int $id
     *
     * @return Response
     */

    public function delete(UserGenerator $userGenerator, int $id)
    {
        $data = $userGenerator->delete( $id);
        $this->addFlash('success', $data);
        $this->addFlash('success', $data);
        return new Response($data, 200, array(
            'Content-Type' => 'application/json'
        ));
    }

    /**
     * @Route(
     *     "/putuser/{id}",
     *     name="putuser",
     *     methods={"PUT"},
     *     requirements={"id"="\d+"}
     * )
     *
     *
     * @return Response
     */
    public function putUser  (UserGenerator $userGenerator, request $request, int $id,UserPasswordEncoderInterface $encoder)
    {
        $data = $userGenerator->putUser($request, $id,$encoder);
        $this->addFlash('success', $data);
        return new Response($data, 200, array(
            'Content-Type' => 'application/json'
        ));

    }

}
