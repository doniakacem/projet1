<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FOSRestController extends AbstractController
{
    /**
     * @Route("/f/o/s/rest", name="f_o_s_rest")
     */
    public function index()
    {
        return $this->render('fos_rest/index.html.twig', [
            'controller_name' => 'FOSRestController',
        ]);
    }
}
