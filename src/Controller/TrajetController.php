<?php

namespace App\Controller;

use App\Service\TrajetService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;

class TrajetController extends AbstractController
{


    private $trajetservice;

    public function __construct(TrajetService $trajetservice)
    {
        $this->trajetservice=$trajetservice;
    }
    /**
     *
     * @return string
     * lists all the project
     * @Rest\Get("/alltrajets")
     */
    public function getAllTrajet()
    {
        $data = $this->trajetservice->getAlltrajet();

        $reponse = $this->get('serializer')->serialize($data,'json');
        return new Response($reponse, 200, array(
            'Content-Type' => 'application/json'
        ));
        return $reponse;
    }
    /**
     * @Rest\Post("/ajouttrajet", name="ajouttrajet")
     *@param TrajetService $trajetservice
     * @param Request $request
     *
     * @return Response
     */
    public function AjoutTrajet(TrajetService $trajetService, request $request)
    {
        $data = $trajetService->AjoutTrajet($request);
        $this->addFlash('ajout avec success', $data);
        return new Response($data, 200, array(
            'Content-Type' => 'application/json'
        ));

    }
    /**
     * @Route(
     *     "/deletetrajet/{id}",
     *     name="deletetrajet",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     * @param int $id
     *
     * @return Response
     */

    public function delete(TrajetService $trajetService, int $id)
    {
        $data = $trajetService->delete( $id);
        $this->addFlash('success', $data);
        return new Response($data, 200, array(
            'Content-Type' => 'application/json'
        ));


    }
    /**
     * @Route(
     *     "/puttrajet/{id}",
     *     name="puttrajet",
     *     methods={"PUT"},
     *     requirements={"id"="\d+"}
     * )
     *
     *
     * @return Response
     */
    public function putTrajet  (TrajetService $trajetService, request $request, int $id)
    {
        $data = $trajetService->putTrajet($request, $id);
        $this->addFlash('success', $data);
        return new Response($data, 200, array(
            'Content-Type' => 'application/json'
        ));

    }


}
