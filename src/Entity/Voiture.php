<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VoitureRepository")
 */
class Voiture
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $matricule;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbrplace;



    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Modelevoiture", inversedBy="voitures")
     */
    private $modele;





    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Annonce", mappedBy="voiture")
     */
    private $annonces;

    /**
     * @ORM\Column(type="boolean")
     */
    private $climatisee;

    public function __construct()
    {
        $this->annonces = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatricule(): ?string
    {
        return $this->matricule;
    }

    public function setMatricule(string $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

    public function getNbrplace(): ?int
    {
        return $this->nbrplace;
    }

    public function setNbrplace(int $nbrplace): self
    {
        $this->nbrplace = $nbrplace;

        return $this;
    }


    public function getModele(): ?Modelevoiture
    {
        return $this->modele;
    }

    public function setModele(?Modelevoiture $modele): self
    {
        $this->modele = $modele;

        return $this;
    }



    /**
     * @return Collection|Annonce[]
     */
    public function getAnnonces(): Collection
    {
        return $this->annonces;
    }

    public function addAnnonce(Annonce $annonce): self
    {
        if (!$this->annonces->contains($annonce)) {
            $this->annonces[] = $annonce;
            $annonce->setVoiture($this);
        }

        return $this;
    }

    public function removeAnnonce(Annonce $annonce): self
    {
        if ($this->annonces->contains($annonce)) {
            $this->annonces->removeElement($annonce);
            // set the owning side to null (unless already changed)
            if ($annonce->getVoiture() === $this) {
                $annonce->setVoiture(null);
            }
        }

        return $this;
    }

    public function getClimatisee(): ?bool
    {
        return $this->climatisee;
    }

    public function setClimatisee(bool $climatisee): self
    {
        $this->climatisee = $climatisee;

        return $this;
    }
}
