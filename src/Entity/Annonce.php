<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnnonceRepository")
 */
class Annonce
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pointdepart;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pointarrivee;

    /**
     * @ORM\Column(type="string")
     */
    private $datedepart;

    /**
     * @ORM\Column(type="string")
     */
    private $heuredepart;

    /**
     * @ORM\Column(type="float")
     */
    private $prix;

    /**
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $musique;

    /**
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $pause;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Trajet", inversedBy="annonces")
     */
    private $trajet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Conducteur", inversedBy="annonces")
     */
    private $conducteur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Voiture", inversedBy="annonces")
     */
    private $voiture;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="annonces")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Avis", mappedBy="annonce")
     */
    private $avis;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reservation", mappedBy="annonce")
     */
    private $reservations;

    /**
     * @ORM\Column(type="boolean")
     */
    private $bagage;

    public function __construct()
    {
        $this->user = new ArrayCollection();
        $this->avis = new ArrayCollection();
        $this->reservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPointdepart(): ?string
    {
        return $this->pointdepart;
    }

    public function setPointdepart(string $pointdepart): self
    {
        $this->pointdepart = $pointdepart;

        return $this;
    }

    public function getPointarrivee(): ?string
    {
        return $this->pointarrivee;
    }

    public function setPointarrivee(string $pointarrivee): self
    {
        $this->pointarrivee = $pointarrivee;

        return $this;
    }

    public function getDatedepart(): ?\DateTimeInterface
    {
        return $this->datedepart;
    }

    public function setDatedepart($datedepart): self
    {
        $this->datedepart = $datedepart;

        return $this;
    }

    public function getHeuredepart()
    {
        return $this->heuredepart;
    }

    public function setHeuredepart( $heuredepart): self
    {
        $this->heuredepart = $heuredepart;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getMusique(): ?bool
    {
        return $this->musique;
    }

    public function setMusique(bool $musique): self
    {
        $this->musique = $musique;

        return $this;
    }

    public function getPause(): ?bool
    {
        return $this->pause;
    }

    public function setPause(bool $pause): self
    {
        $this->pause = $pause;

        return $this;
    }

    public function getTrajet()
    {
        return $this->trajet;
    }

    public function setTrajet($trajet)
    {
        $this->trajet = $trajet;

        return $this;
    }

    public function getConducteur(): ?Conducteur
    {
        return $this->conducteur;
    }

    public function setConducteur(?Conducteur $conducteur): self
    {
        $this->conducteur = $conducteur;

        return $this;
    }

    public function getVoiture(): ?Voiture
    {
        return $this->voiture;
    }

    public function setVoiture(?Voiture $voiture): self
    {
        $this->voiture = $voiture;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
        }

        return $this;
    }

    /**
     * @return Collection|Avis[]
     */
    public function getAvis(): Collection
    {
        return $this->avis;
    }

    public function addAvi(Avis $avi): self
    {
        if (!$this->avis->contains($avi)) {
            $this->avis[] = $avi;
            $avi->setAnnonce($this);
        }

        return $this;
    }

    public function removeAvi(Avis $avi): self
    {
        if ($this->avis->contains($avi)) {
            $this->avis->removeElement($avi);
            // set the owning side to null (unless already changed)
            if ($avi->getAnnonce() === $this) {
                $avi->setAnnonce(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setAnnonce($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->contains($reservation)) {
            $this->reservations->removeElement($reservation);
            // set the owning side to null (unless already changed)
            if ($reservation->getAnnonce() === $this) {
                $reservation->setAnnonce(null);
            }
        }

        return $this;
    }

    public function getBagage(): ?bool
    {
        return $this->bagage;
    }

    public function setBagage(bool $bagage): self
    {
        $this->bagage = $bagage;

        return $this;
    }




}
