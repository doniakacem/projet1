<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TrajetRepository")
 */
class Trajet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Villedepart;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $villearrivee;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Annonce", mappedBy="trajet")
     */
    private $annonces;

    public function __construct()
    {
        $this->annonces = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVilledepart(): ?string
    {
        return $this->Villedepart;
    }

    public function setVilledepart(string $Villedepart): self
    {
        $this->Villedepart = $Villedepart;

        return $this;
    }

    public function getVillearrivee(): ?string
    {
        return $this->villearrivee;
    }

    public function setVillearrivee(string $villearrivee): self
    {
        $this->villearrivee = $villearrivee;

        return $this;
    }

    /**
     * @return Collection|Annonce[]
     */
    public function getAnnonces(): Collection
    {
        return $this->annonces;
    }

    public function addAnnonce(Annonce $annonce): self
    {
        if (!$this->annonces->contains($annonce)) {
            $this->annonces[] = $annonce;
            $annonce->setTrajet($this);
        }

        return $this;
    }

    public function removeAnnonce(Annonce $annonce): self
    {
        if ($this->annonces->contains($annonce)) {
            $this->annonces->removeElement($annonce);
            // set the owning side to null (unless already changed)
            if ($annonce->getTrajet() === $this) {
                $annonce->setTrajet(null);
            }
        }

        return $this;
    }
}
