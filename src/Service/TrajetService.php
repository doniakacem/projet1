<?php


namespace App\Service;


use App\Entity\Trajet;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class TrajetService
{
    private $em;


    public function __construct(EntityManagerInterface $em, HttpClientInterface $client)
    {
        $this->client = $client;
        $this->em = $em;

    }
    public function getAlltrajet()
    {
        $repository = $this->em->getRepository(Trajet::class);
        $trajet = $repository->findall();
        return $trajet;
    }
    public function AjoutTrajet (Request $request)
    {

        $trajet = new Trajet();
        $villedepart = $request->request->get('villedepart');
        $villearrivee = $request->get("villearrivee");


        $errors = [];
        if ((!$villearrivee) || (!$villedepart)) {
            $errors[] = "tajet  introuvable";
        }
        if (!$errors) {


            $trajet->setVilledepart($villedepart);
            $trajet->setVillearrivee($villearrivee);

            try {
                $this->em->persist($trajet);
                $this->em->flush();

                return new JsonResponse([
                    'trajet' => $trajet
                ], JsonResponse::HTTP_CREATED);
            } catch (UniqueConstraintViolationException $e) {

                dd($e->getMessage());

            }
            return new JsonResponse(['errors' => $errors], 400);
        }

    }
    public function delete(int $id)
    {
        $repository = $this->em->getRepository(Trajet::class);
        $trajet = $repository->findOneById($id);

        $this->em->remove($trajet);
        $this->em->flush();

        return new JsonResponse(
            null,
            JsonResponse::HTTP_NO_CONTENT
        );
    }
    public function putTrajet(Request $request, int $id){
        $repository = $this->em->getRepository(Trajet::class);
        $trajet = $repository->find($id);
        if (empty($trajet)) {
            return new JsonResponse(
                [
                    'status' => 'no',
                ],
                JsonResponse::HTTP_NOT_FOUND
            );
        }
        try {
            $villedepart = $request->request->get('villedepart');
            $villearrivee = $request->get("villearrivee");
            $trajet->setVilledepart($villedepart);
            $trajet->setVillearrivee($villearrivee);
            $this->em->persist($trajet);
            $this->em->flush();

            return new JsonResponse([
                'trajet' => $trajet
            ], JsonResponse::HTTP_CREATED);
        } catch (UniqueConstraintViolationException $e) {

            dd($e->getMessage());

        }
        return new JsonResponse( 400);
    }

}