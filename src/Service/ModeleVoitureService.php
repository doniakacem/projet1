<?php


namespace App\Service;


use App\Entity\Modelevoiture;
use App\Entity\Type;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ModeleVoitureService
{
    private $em;


    public function __construct(EntityManagerInterface $em, HttpClientInterface $client)
    {
        $this->client = $client;
        $this->em = $em;

    }

    public function getAllVoitures()
    {
        $repository = $this->em->getRepository(Modelevoiture::class);
        $modelevoitures = $repository->findall();
        return $modelevoitures;
    }

    public function AjoutModele (Request $request)
    {

        $modelevoiture = new Modelevoiture();
        $type = $request->request->get('type');
        $modele = $request->get("modele");


        $errors = [];
        if (!$modele) {
            $errors[] = "Modele introuvable";
        }
        if (!$errors) {


            $modelevoiture->setType($type);
            $modelevoiture->setModele($modele);

            try {
                $this->em->persist($modelevoiture);
                $this->em->flush();

                return new JsonResponse([
                    'modelevoiture' => $modelevoiture
                ], JsonResponse::HTTP_CREATED);
            } catch (UniqueConstraintViolationException $e) {

                dd($e->getMessage());

            }
            return new JsonResponse(['errors' => $errors], 400);
        }

    }


    public function getmodele()
    {
        $repository = $this->em->getRepository(Modelevoiture::class);
        $modeles = $repository->findAll();
        return $modeles;
    }

    public function gettypebymodele($idmodele)
    {
        $repository = $this->em->getRepository(Type::class);
        $modeles = $repository->findBy(['modelevoiture'=>$idmodele]);
        return $modeles;
    }

    public function delete(int $id)
    {
        $repository = $this->em->getRepository(Modelevoiture::class);
        $modeles = $repository->findOneById($id);

        $this->em->remove($modeles);
        $this->em->flush();

        return new JsonResponse(
            null,
            JsonResponse::HTTP_NO_CONTENT
        );
    }



    public function putModele(Request $request, int $id){
        $repository = $this->em->getRepository(Modelevoiture::class);
        $modeles = $repository->find($id);
        if (empty($modeles)) {
            return new JsonResponse(
                [
                    'status' => 'no',
                ],
                JsonResponse::HTTP_NOT_FOUND
            );
        }
        try {
            $type = $request->request->get('type');
            $modele = $request->get("modele");
            $modeles->setType($type);
            $modeles->setModele($modele);
            $this->em->persist($modeles);
            $this->em->flush();

            return new JsonResponse([
                'modele' => $modeles
            ], JsonResponse::HTTP_CREATED);
        } catch (UniqueConstraintViolationException $e) {

            dd($e->getMessage());

        }
        return new JsonResponse( 400);
    }





}