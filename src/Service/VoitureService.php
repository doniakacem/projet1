<?php


namespace App\Service;


use App\Entity\Modelevoiture;
use App\Entity\Voiture;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class VoitureService
{
    private $em;


    public function __construct(EntityManagerInterface $em, HttpClientInterface $client)
    {
        $this->client = $client;
        $this->em = $em;

    }

    public function getAllvoiture()
    {

        $repository = $this->em->getRepository(Modelevoiture::class);
        $voiture = $repository->findall();
        return $voiture;
    }

    public function AjoutVoiture(Request $request)
    {

        $voiture = new Voiture();
        $matricule = $request->request->get('matricule');
        $nbrplace = $request->get("nbrplace");
        $modele = $this->em->getRepository(Modelevoiture::class)
            ->findOneBy(["id" => $request->get("modele"),
                "type" => $request->get("type")]);

        $climatisee = $request->get("climatisee");
        $voiture->setMatricule($matricule);
        $voiture->setNbrplace($nbrplace);
        $voiture->setModele($modele);

        $voiture->setClimatisee($climatisee);

        try {
            $this->em->persist($voiture);
            $this->em->flush();

            return new JsonResponse([
                'voiture' => $voiture
            ], JsonResponse::HTTP_CREATED);
        } catch (UniqueConstraintViolationException $e) {

            dd($e->getMessage());

        }
        return new JsonResponse(400);
    }

    public function delete(int $id)
    {
        $repository = $this->em->getRepository(Voiture::class);
        $voiture = $repository->findOneById($id);

        $this->em->remove($voiture);
        $this->em->flush();

        return new JsonResponse(
            null,
            JsonResponse::HTTP_NO_CONTENT
        );
    }

    public function putVoiture(Request $request, int $id)
    {
        $repository = $this->em->getRepository(Voiture::class);
        $voiture = $repository->find($id);
        if (empty($voiture)) {
            return new JsonResponse(
                [
                    'status' => 'voiture introuvable',
                ],
                JsonResponse::HTTP_NOT_FOUND
            );
        }
        try {
            $matricule = $request->request->get('matricule');
            $nbrplace = $request->get("nbrplace");
            $modele = $this->em->getRepository(Modelevoiture::class)->findOneBy($id);
            $voiture->setMatricule($matricule);
            $voiture->setNbrplace($nbrplace);
            $voiture->setModele($modele);
            $this->em->persist($voiture);
            $this->em->flush();

            return new JsonResponse([
                'voiture' => $voiture
            ], JsonResponse::HTTP_CREATED);
        } catch (UniqueConstraintViolationException $e) {

            dd($e->getMessage());

        }
        return new JsonResponse(400);
    }


}