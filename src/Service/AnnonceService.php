<?php


namespace App\Service;


use App\Entity\Annonce;
use App\Entity\Trajet;
use App\Entity\User;
use App\Entity\Voiture;
use DateTime;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AnnonceService
{

    private $em;



    public function __construct(EntityManagerInterface $em, HttpClientInterface $client)
    {
        $this->client = $client;
        $this->em = $em;
    }

    public function Postuler(Request $request)
    {

        $annonce = new Annonce();
        $searchtrajet = $this->em->getRepository(Trajet::class)->findOneBy(["Villedepart" => $request->get("villedepart"),
            "villearrivee" => $request->get("villearrivee")]);
        if (!empty($searchtrajet)) {
            $annonce->setTrajet($searchtrajet);
        } else {
            $trajet = new Trajet();
            $trajet->setVilledepart($request->get("villedepart"));
            $trajet->setVillearrivee($request->get("villearrivee"));
            $annonce->setTrajet($trajet);
            $this->em->persist($trajet);

        }
        //$voiture = new Voiture();
        //$voiture->setNbrplace($request->get("nbrplace"));
        $pointdepart = $request->get("pointdepart");
        $pointarrivee = $request->get("pointarrivee");
        $datedepart = $request->get("datedepart");
        $heuredepart = $request->get("heuredepart");
        $prix = $request->get("prix");
        $musique = $request->get("musique");
        $pause = $request->get("pause");
        $bagage = $request->get("bagage");


        $annonce->setDatedepart($datedepart);
        $annonce->setHeuredepart($heuredepart);
        $annonce->setPrix($prix);
        $annonce->setMusique($musique);
        $annonce->setPause($pause);
        $annonce->setBagage($bagage);
        $annonce->setPointdepart($pointdepart);
        $annonce->setPointarrivee($pointarrivee);
        //$annonce->setVoiture($voiture);
        //dump($annonce);
        //dd("hi");
        try {
            $this->em->persist($annonce);
            $this->em->flush();

            return new JsonResponse([
                'annonce' => $annonce
            ], JsonResponse::HTTP_CREATED);
        } catch (UniqueConstraintViolationException $e) {

            dd($e->getMessage());

        }
        return new JsonResponse(400);
    }


}