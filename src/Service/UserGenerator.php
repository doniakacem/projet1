<?php


namespace App\Service;


use App\Entity\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class UserGenerator
{
    private $encoder;
    private $serializer;
    private $em;
    private $form;

    public function __construct(EntityManagerInterface $em, HttpClientInterface $client,SerializerInterface $serializer, UserPasswordEncoderInterface $encoder, FormFactoryInterface $form)
    {
        $this->UserPasswordEncoderInterface = $encoder;
        $this->em = $em;
        $this->serializer = $serializer;
        $this->form = $form;
        $this->client = $client;
    }


    public function inscription(Request $request, UserPasswordEncoderInterface $encoder)
    {

        $user = new User();
        //$username = $request->request->get('username');
        //$genre = $request->get("genre");
        //$numtel = $request->get("numtel");
        $email = $request->get("email");

        $password = $request->request->get("password");
        $passwordConfirmation = $request->request->get("confirmpassword");


        $errors = [];
        if ($password != $passwordConfirmation) {


            $errors[] = "Password does not match the password confirmation.";


        }
        if (strlen($password) < 6) {
            $errors[] = "Password should be at least 6 characters.";

        }
        if (!$errors) {

            $encodedPassword = $encoder->encodePassword($user, $password);
            $user->setEmail($email);
            //$user->setNumtel($numtel);
            $user->setPassword($encodedPassword);
            $user->setConfirmPassword($passwordConfirmation);
            //$user->setUsername($username);
            //$user->setGenre($genre);

            try {
                $this->em->persist($user);
                $this->em->flush();

                return new JsonResponse([
                    'user' => $user
                ], JsonResponse::HTTP_CREATED);
            } catch (UniqueConstraintViolationException $e) {
                $errors[] = "The email provided already has an account!";
                dd($e->getMessage());

            } catch (\Exception $e) {
                dd($e->getMessage());

                $errors[] = "Unable to save new user at this time.";
            }
        }
        return new JsonResponse(['errors' => $errors], 400);
    }


    public function getAllUsers()
    {
        $repository = $this->em->getRepository(User::class);
        $users = $repository->findall();
        return $users;
    }


    public function delete($id)
    {
        $repository = $this->em->getRepository(User::class);
        $users = $repository->findOneById($id);

        $this->em->remove($users);
        $this->em->flush();

        return new JsonResponse(
            null,
            JsonResponse::HTTP_NO_CONTENT
        );
    }

    public function putUser(Request $request, int $id, UserPasswordEncoderInterface $encoder){
        $repository = $this->em->getRepository(User::class);
        $user = $repository->find($id);
        if (empty($user)) {
            return new JsonResponse(
                [
                    'status' => 'no',
                ],
                JsonResponse::HTTP_NOT_FOUND
            );
        }
        try {
            $username = $request->request->get('username');
            $genre = $request->get("genre");
            $numtel = $request->get("numtel");
            $email = $request->get("email");
            $password = $request->request->get("password");
            $passwordConfirmation = $request->request->get("confirmpassword");
            $encodedPassword = $encoder->encodePassword($user, $password);
            $user->setEmail($email);
            $user->setNumtel($numtel);
            $user->setPassword($encodedPassword);
            $user->setConfirmPassword($passwordConfirmation);
            $user->setUsername($username);
            $user->setGenre($genre);
            $this->em->persist($user);
            $this->em->flush();

            return new JsonResponse([
                'user' => $user
            ], JsonResponse::HTTP_CREATED);
        } catch (UniqueConstraintViolationException $e) {

            dd($e->getMessage());

        }
        return new JsonResponse( 400);
    }

}