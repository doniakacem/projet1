<?php

namespace App\Repository;

use App\Entity\Modelevoiture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Modelevoiture|null find($id, $lockMode = null, $lockVersion = null)
 * @method Modelevoiture|null findOneBy(array $criteria, array $orderBy = null)
 * @method Modelevoiture[]    findAll()
 * @method Modelevoiture[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModelevoitureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Modelevoiture::class);
    }

    // /**
    //  * @return Modelevoiture[] Returns an array of Modelevoiture objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Modelevoiture
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
